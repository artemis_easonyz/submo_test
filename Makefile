SHELL = bash
PackagePath = $(shell pwd)

SUBTEST_PATH ?= ../../
TEST_EXE = bin/testmain
LIBRARY_ATABLE_SO_FILE = dsat-eason/lib/libdsat.so
LIBRARY_BUEXCEPTION = buexception/lib/libToolException.so

INCLUDE_PATH = \
				-Idsat-eason/include \
				-Itclap/include
LIBRARY_PATH = \
				-Ldsat-eason/lib \
				-Lbuexception/lib

CPP_FLAGS = -std=c++11 -g -O3 -rdynamic -Wall -MMD -MP -fPIC ${INCLUDE_PATH} -Werror -Wno-literal-suffix
CPP_FLAGS +=-fno-omit-frame-pointer -Wno-ignored-qualifiers -Werror=return-type -Wextra -Wno-long-long -Winit-self -Wno-unused-local-typedefs  -Woverloaded-virtual
LINK_LIBRARY_FLAGS = -Xlinker "--no-as-needed" -shared -fPIC -Wall -g -O3 -rdynamic ${LIBRARY_PATH} -Wl,-rpath=${PackagePath}/lib
LD_FLAGS           = -Xlinker "--no-as-needed"               -Wall -g -O3 -rdynamic ${LIBRARY_PATH} -Wl,-rpath=${PackagePath}/lib

defult: build
clean: _cleanall
_cleanall:
	rm -rf obj
	rm -rf bin
	rm -rf dsat-eason/lib
	rm -rf dsat-eason/obj
	rm -rf buexception/lib
	rm -rf buexception/obj

all: _all
build: _all
buildall: _all
_all: ${TEST_EXE}

${LIBRARY_ATABLE_SO_FILE}:
	${MAKE} -C dsat-eason


${TEST_EXE}:obj/testmain.o ${LIBRARY_ATABLE_SO_FILE}
	mkdir -p ${dir $@}
	g++ ${LD_FLAGS} -ldsat -lToolException $< -o $@ 

obj/%.o : src/%.cxx
	mkdir -p $(dir $@)
	g++ ${CPP_FLAGS} -c $< -o $@
