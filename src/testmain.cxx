#include<AddressTable.hh>
//#include<AddressTableException.hh>
//#include<AddressTable_ItemConversion.hh>
//#include<ExceptionBase.hh>
#include<fstream>
#include<iostream>
#include<stdlib.h>
#include<algorithm>
#include"tclap/CmdLine.h"

int main(int argc, char** argv){
  try {//quite confuse about why this try catch block doesn't require "throw"
    TCLAP::CmdLine cmd("Command description message", ' ', "0.9");
    
    TCLAP::ValueArg<std::string> nameArg("n","name","Name to print",true,"homer","string");
    cmd.add( nameArg );

    cmd.parse( argc, argv );

    std::string tablename = nameArg.getValue();
    
    AddressTable testtable(tablename);
    std::vector<std::string> name1 = testtable.GetNames();
    for (size_t i = 0; i < name1.size(); i++){
      std::cout<<name1[i]<<std::endl;
    }

  }catch(TCLAP::ArgException &e)
    {std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; }
 
}

